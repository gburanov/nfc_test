/*
 * utils.cpp
 *
 *  Created on: Jan 20, 2013
 *      Author: gburanov
 */

#include "utils.h"

#ifdef __APPLE__
#include <libkern/OSByteOrder.h>
#else
#include <byteswap.h>
#endif

uint32_t ByteSwap(uint32_t v)
{
#ifdef __APPLE__
  return OSSwapInt32(v);
#else
  return bswap_32(v);
#endif
}

