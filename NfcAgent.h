/*
 * NfcAgent.h
 *
 *  Created on: Jan 20, 2013
 *      Author: gburanov
 */

#ifndef NFCAGENT_H_
#define NFCAGENT_H_

#include "NfcDevice.h"

#include <nfc/nfc.h>

#include <string>
#include <memory>

namespace Nfc
{

class NfcAgent
{
public:
  NfcAgent();

  std::auto_ptr<NfcDevice> Connect();
  std::string GetVersion();

  virtual ~NfcAgent();

private:
  nfc_context* Context;
};

}

#endif /* NFCAGENT_H_ */
