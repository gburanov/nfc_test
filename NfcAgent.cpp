/*
 * NfcAgent.cpp
 *
 *  Created on: Jan 20, 2013
 *      Author: gburanov
 */

#include "NfcAgent.h"

namespace Nfc
{

NfcAgent::NfcAgent()
  : Context(0)
{
  nfc_init(Context);
}

std::auto_ptr<NfcDevice> NfcAgent::Connect()
{
  std::auto_ptr<NfcDevice> ret;
  nfc_device* pnd = nfc_open(Context, NULL);
  if (pnd)
      ret.reset(new NfcDevice(pnd));
  return ret;
}

std::string NfcAgent::GetVersion()
{
  return nfc_version();
}

NfcAgent::~NfcAgent()
{
  nfc_exit(NULL);
}

}

