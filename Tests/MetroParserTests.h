/*
 * MetroParserTests.h
 *
 *  Created on: Jan 16, 2013
 *      Author: gburanov
 */

#ifndef METROPARSERTESTS_H_
#define METROPARSERTESTS_H_

namespace Nfc
{
namespace Tests
{



class MetroParserTests {
public:
	MetroParserTests();
	virtual ~MetroParserTests();

	void Run();
};


}
}

#endif /* METROPARSERTESTS_H_ */
