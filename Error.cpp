/*
 * Error.cpp
 *
 *  Created on: Jan 19, 2013
 *      Author: gburanov
 */

#include "Error.h"

#include <sstream>

namespace Nfc
{

Error::Error()
  : Line(0)
{
    // TODO Auto-generated constructor stub

}

Error::Error(const std::string& filename, int line)
  : Filename(filename)
  , Line(line)
{

}


Error::~Error() throw()
{
    // TODO Auto-generated destructthrow Nfc::Error(__FILE__, __LINE__);or stub
}

std::string Error::GetText() const
{
  std::stringstream ss;
  ss << "Error in file " << Filename << " in line " << Line;
  return ss.str();
}

} /* namespace Nfc */
