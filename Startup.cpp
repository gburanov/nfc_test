/*
 * Startup.cpp
 *
 *  Created on: Feb 3, 2013
 *      Author: gburanov
 */

#include "Startup.h"
#include "Error.h"
#include "NfcAgent.h"
#include "NfcDevice.h"
#include "MetroParser.h"

#include "Tests/tests.h"

#include <stdlib.h>
#include <iostream>

#include <nfc/nfc.h>

#include <getopt.h>

Startup::Startup(int argc, char** argv)
  : NeedToDump(false)
  , RunTests(false)
{
  Init(argc, argv);
  Execute();
}

Startup::~Startup()
{
	// TODO Auto-generated destructor stub
}

void Startup::Init(int argc, char** argv)
{
  option long_options[] =
    {
      { "dump", no_argument, 0, 'd' },
      { "tests", no_argument, 0, 't' },
      { 0, 0, 0, 0 }
    };

  int ret(0);
  while(true)
  {
    int option_index = 0;
    ret = getopt_long(argc, argv, "dt", long_options, &option_index);
    if (ret == -1)
      break;

    switch (ret)
    {
    case 'd':
      NeedToDump = true;
      break;

    case 't':
      RunTests = true;
      break;

    default:
      throw Nfc::Error(__FILE__, __LINE__);
    }
  }
}

void Startup::Execute()
{
  if (RunTests)
  {
      Nfc::Tests::StartTests();
  }
  else
  {
      Nfc::NfcAgent agent;
      std::string version = agent.GetVersion();
      std::cout << "NFC lib version is " << version << std::endl;

      std::auto_ptr<Nfc::NfcDevice> dev = agent.Connect();
      if (!dev.get())
        {
          std::cout << "Unable to get NFC reader" << std::endl;
          throw Nfc::Error(__FILE__, __LINE__);;
        }
      dev->Init();
      std::cout << "Opened NFC reader with name" << dev->GetName() << std::endl;

      std::vector<char> bytes = dev->ReadMifareUltralight();
      std::cout << "Obtained Ultralight data - size " << bytes.size()
          << std::endl;

      Nfc::Metro::MetroParser mp;
      mp.DecodeUltralight(bytes);
      mp.ToString();
      if (NeedToDump)
        mp.MakeDump();
  }

}

