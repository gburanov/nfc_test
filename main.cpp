
#include "Error.h"
#include "Startup.h"

#include <iostream>

int main (int argc, char *argv[])
{
  try
  {
    Startup startup(argc, argv);
    startup.Execute();
  }
  catch (const Nfc::Error& err)
  {
    std::cout << "Oops we got error" << std::endl;
    std::cout << err.GetText();
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
