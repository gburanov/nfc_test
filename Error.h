/*
 * Error.h
 *
 *  Created on: Jan 19, 2013
 *      Author: gburanov
 */

#ifndef ERROR_H_
#define ERROR_H_

#include <string>
#include <stdexcept>

namespace Nfc
{

class Error : public std::exception
{
public:
  Error();
  Error(const std::string& filename, int line);

  virtual ~Error() throw();

  std::string GetText() const;

private:
  std::string Filename;
  int Line;
};

} /* namespace Nfc */
#endif /* ERROR_H_ */
