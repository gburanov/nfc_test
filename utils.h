/*
 * utils.h
 *
 *  Created on: Jan 20, 2013
 *      Author: gburanov
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <stdint.h>

uint32_t ByteSwap(uint32_t v);

#endif /* UTILS_H_ */
