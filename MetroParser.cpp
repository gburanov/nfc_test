/*
 * MetroParser.cpp
 *
 *  Created on: Jan 16, 2013
 *      Author: gburanov
 */

#include "MetroParser.h"
#include "MetroParserDatabase.h"

#include "Error.h"
#include "utils.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

namespace
{
   std::string TicketTypeToString(Nfc::Metro::TicketType type)
   {
     switch (type)
     {
     case Nfc::Metro::MOSMETRO:
       return "Метро";
     case Nfc::Metro::MOSGROUND:
       return "MOSGROUND";
     case Nfc::Metro::MOSUNIVERSAL:
       return "MOSUNIVERSAL";
     case Nfc::Metro::MOSMETROLIGHT:
       return "MOSMETROLIGHT";
     case Nfc::Metro::MOSUNIVERSALTICKET:
       return "Универсальный билет Москва";
       break;
     case Nfc::Metro::UNKNOWN_TICKET:
       break;
     }
     return "Непонятный тип билета";
   }

   std::string CardTypeToString(Nfc::Metro::CardType type)
   {
     switch (type)
     {
     case Nfc::Metro::ONE_TRIP_RIDE:
       return "Билет на одну поездку";
     case Nfc::Metro::TWO_TRIP_RIDE:
       return "Билет на две поездки";
     case Nfc::Metro::THREE_TRIP_RIDE:
       return "Билет на три поездки";
     case Nfc::Metro::FOUR_TRIP_RIDE:
       return "Билет на четыре поездки";
     case Nfc::Metro::FIVE_TRIP_RIDE:
       return "Билет на пять поездок";
     case Nfc::Metro::TEN_TRIP_RIDE:
       return "Билет на десять поездок";
     case Nfc::Metro::TWENTY_TRIP_RIDE:
       return "Билет на двадцать поездок";
     case Nfc::Metro::SIXTY_TRIP_RIDE:
       return "Билет на шестьдесят поездок";
     case Nfc::Metro::BAGGAGE_AND_PASS:
       return "Багаж и билет";
     case Nfc::Metro::BAGGAGE_ONLY:
       return "Только багаж";
     case Nfc::Metro::UNIVERSAL_ULTRALIGHT_70:
       return "UNIVERSAL_ULTRALIGHT_70";
     case Nfc::Metro::VESB:
       return "VESB";
     case Nfc::Metro::UNKNOWN_TYPE:
       break;
     }
     return "Непонятный тип карты";
   }

   std::string LastStationToString(uint16_t station)
   {
     return Nfc::Metro::MetroParserDatabase().GetStationName(station);
   }


}

namespace Nfc
{
namespace Metro
{

MetroParser::MetroParser()
  : TType(UNKNOWN_TICKET)
  , CType(UNKNOWN_TYPE)
  , CardNumber(0)
  , CardLayout(0)
  , RidesLeft(0)
  , IssuedAt(0)
  , BecomeInvalidTime(0)
  , LastStation(0)
  , Hash(0)
{
}

MetroParser::~MetroParser()
{
}

void MetroParser::DecodeUltralight(std::vector<char>& bytes)
{
  Dump = bytes;
  if (Dump.size() != 64)
    throw Nfc::Error(__FILE__, __LINE__);

  TType = DecodeTicketType();
  CType = DecodeCardType();
  CardNumber = DecodeCardNumber();
  CardLayout = DecodeCardLayout();
  RidesLeft = DecodeRidesLeft();
  IssuedAt = DecodeIssuedAt();
  BecomeInvalidTime = GetBecomeInvalidTime();
  LastStation =  DecodeStationLastEnter();
  Hash = DecodeHash();
}

TicketType MetroParser::DecodeTicketType()
{
  TicketType ret(UNKNOWN_TICKET);
  uint32_t* int_array = reinterpret_cast<uint32_t*>(&Dump[0]);
  uint32_t p4 = ByteSwap(*(int_array + 4));
  p4 = p4 >> 22;
  switch (p4)
  {
  case 262:
    return MOSMETRO;
  case 264:
    return MOSGROUND;
  case 266:
    return MOSUNIVERSAL;
  case 270:
    return MOSMETROLIGHT;
  case 279:
    return MOSUNIVERSALTICKET;
  default:
    std::cout << "Unknown ticket type: " << p4 << std::endl;
    break;
  }
  return ret;
}

void MetroParser::ToString()
{
  std::cout << "Ticket information" << std::endl;
  std::cout << "Card Number " << CardNumber << std::endl;
  std::cout << "TicketType " << TicketTypeToString(TType) << std::endl;
  std::cout << "CardType "   << CardTypeToString(CType) << std::endl;
  std::cout << "Rides Left " << RidesLeft << std::endl;
  std::cout << "Last visited station " << LastStationToString(LastStation) << std::endl;

  std::cout << "Issued at " << ctime(&IssuedAt);
  std::cout << "Will become invalid at " << ctime(&BecomeInvalidTime);
}

void MetroParser::MakeDump()
{
  std::stringstream fileName;
  fileName << CardNumber << ".dmp";
  std::ofstream oFile(fileName.str().c_str(), std::ios::binary|std::ios::out);
  std::cout << "Dumping data to file " << fileName.str() << "...";
  oFile.write(&Dump[0], Dump.size());
  oFile.close();
  std::cout << "Done." << std::endl;
}

CardType MetroParser::DecodeCardType()
{
  CardType ret(UNKNOWN_TYPE);
  uint32_t* int_array = reinterpret_cast<uint32_t*>(&Dump[0]);
  uint32_t p4 = ByteSwap(*(int_array + 4));
  p4 = (p4 >> 12) & 0x3ff;
  switch (p4)
  {
  case 120:
  case 279:
    return ONE_TRIP_RIDE;
  case 121:
  case 412:
    return TWO_TRIP_RIDE;
  case 122:
    return THREE_TRIP_RIDE;
  case 123:
    return FOUR_TRIP_RIDE;
  case 126:
    return FIVE_TRIP_RIDE;
  case 164:
  case 127:
    return TEN_TRIP_RIDE;
  case 128:
    return TWENTY_TRIP_RIDE;
  case 129:
  case 166:
    return SIXTY_TRIP_RIDE;
  case 130:
    return BAGGAGE_AND_PASS;
  case 131:
    return BAGGAGE_ONLY;
  case 149:
    return UNIVERSAL_ULTRALIGHT_70;
  case 150:
    return VESB;
  default:
    std::cout << "Unknown card type: " << p4 << std::endl;
    break;
  }

  return ret;
}

long MetroParser::DecodeCardNumber()
{
  uint32_t* int_array = reinterpret_cast<uint32_t*>(&Dump[0]);
  uint32_t p4 = ByteSwap(*(int_array + 4));
  uint32_t p5 = ByteSwap(*(int_array + 5));
  long cardId = ((p4 & 0xfff) << 20) | (p5 >> 12);
  return cardId;
}

uint8_t MetroParser::DecodeCardLayout()
{
  uint32_t* int_array = reinterpret_cast<uint32_t*>(&Dump[0]);
  uint32_t p5 = ByteSwap(*(int_array + 5));
  return ((p5 >> 8) & 0xf);
}

uint16_t MetroParser::DecodeRidesLeft()
{
  uint32_t* int_array = reinterpret_cast<uint32_t*>(&Dump[0]);
  uint32_t p9 = ByteSwap(*(int_array + 9));
  return p9 >> 16;
}

time_t MetroParser::DecodeIssuedAt()
{
  uint32_t* int_array = reinterpret_cast<uint32_t*>(&Dump[0]);
  uint32_t p8 = ByteSwap(*(int_array + 8));
  return GetTime(p8 >> 16);
}

time_t MetroParser::GetBecomeInvalidTime()
{
  time_t ret = DecodeIssuedAt();
  ret += (86400 * DecodeBestInDays());
  return ret;
}

uint8_t MetroParser::DecodeBestInDays()
{
  uint32_t* int_array = reinterpret_cast<uint32_t*>(&Dump[0]);
  uint32_t p8 = ByteSwap(*(int_array + 8));
  return (p8 >> 8) & 0xff;
}

time_t MetroParser::GetTime(int daysFrom1992)
{
  time_t rawtime;
  time(&rawtime);

  tm* startTime = localtime(&rawtime);
  startTime->tm_year = 92;
  startTime->tm_mon = 0;
  startTime->tm_mday = 1;
  time_t start = mktime(startTime);

  start += (86400 * daysFrom1992); // 86400 seconds in day

  return start;
}

uint16_t MetroParser::DecodeStationLastEnter()
{
  uint32_t* int_array = reinterpret_cast<uint32_t*>(&Dump[0]);
  uint32_t p9 = ByteSwap(*(int_array + 9));
  uint16_t ret = p9 & 0xffff;
  return ret;
}

uint32_t MetroParser::DecodeHash()
{
  uint32_t* int_array = reinterpret_cast<uint32_t*>(&Dump[0]);
  uint32_t p10 = ByteSwap(*(int_array + 10));
  return p10;
}

time_t MetroParser::DecodeBlankBestbefore()
{
  uint32_t* int_array = reinterpret_cast<uint32_t*>(&Dump[0]);
  uint32_t p5 = ByteSwap(*(int_array + 5));
  return GetTime((p5 & 0xff) << 8);
}



}
}

