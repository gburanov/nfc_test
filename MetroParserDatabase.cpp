/*
 * MetroParserDatabase.cpp
 *
 *  Created on: Feb 3, 2013
 *      Author: gburanov
 */

#include "MetroParserDatabase.h"

#include <sstream>

namespace Nfc
{
namespace Metro
{

  MetroParserDatabase::MetroParserDatabase()
  {
    // TODO Auto-generated constructor stub

  }

  MetroParserDatabase::~MetroParserDatabase()
  {
    // TODO Auto-generated destructor stub
  }

  std::string MetroParserDatabase::GetStationName(int station)
  {
    static std::map<int, std::string> Stations;
    if (Stations.empty())
      InitStations(Stations);

    std::map<int, std::string>::const_iterator it = Stations.find(station);
    if (it != Stations.end())
    {
      return it->second;
    }
    std::stringstream ss;
    ss << "Турникет " << station << " еще не известен.";
    return ss.str();
  }

  void MetroParserDatabase::InitStations(std::map<int, std::string>& stations)
  {
    // todo: read write to real database
    stations.insert(std::make_pair(494, "Медведково"));
    stations.insert(std::make_pair(742, "Бауманская"));
    stations.insert(std::make_pair(1230, "Парк культуры (радиальная)"));
    stations.insert(std::make_pair(2290, "Парк культуры (радиальная)"));
    stations.insert(std::make_pair(1228, "Авиамоторная"));
    stations.insert(std::make_pair(1228, "Авиамоторная"));
    stations.insert(std::make_pair(2194, "Юго-Западная"));
    stations.insert(std::make_pair(2233, "Спортивная"));
    stations.insert(std::make_pair(2189, "Новокузнецкая"));
    stations.insert(std::make_pair(2522, "Павелецкая"));
    stations.insert(std::make_pair(2645, "Тимирязевская"));
    stations.insert(std::make_pair(2252, "Автозаводская"));
  }

}
}
