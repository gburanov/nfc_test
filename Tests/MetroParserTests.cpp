/*
 * MetroParserTests.cpp
 *
 *  Created on: Jan 16, 2013
 *      Author: gburanov
 */

#include "MetroParserTests.h"

#include "../Error.h"
#include "../MetroParser.h"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace
{

std::vector<char> GetDump(const std::string& dumpFileName)
{
  std::vector<char> ret;
  std::ifstream file(dumpFileName.c_str(),
      std::ios::in | std::ios::binary | std::ios::ate);
  if (!file.is_open())
    throw Nfc::Error(__FILE__, __LINE__);

  size_t size = file.tellg();
  ret.resize(size);

  file.seekg(0, std::ios::beg);
  file.read(&ret[0], size);
  file.close();

  return ret;
}

}

namespace Nfc
{
namespace Tests
{

MetroParserTests::MetroParserTests()
{
	// TODO Auto-generated constructor stub

}

MetroParserTests::~MetroParserTests()
{
	// TODO Auto-generated destructor stub
}

void MetroParserTests::Run()
{
  std::string file("/home/gburanov/eclipse/nfc_test/Tests/Data/metro2.dmp");
  std::vector<char> dump = GetDump(file);
  Metro::MetroParser parser;
  parser.DecodeUltralight(dump);

}


}
}

