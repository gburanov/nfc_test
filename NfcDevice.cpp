/*
 * NfcDevice.cpp
 *
 *  Created on: Jan 20, 2013
 *      Author: gburanov
 */

#include "NfcDevice.h"
#include "Error.h"

#include "mifare.h"

#include <string.h>

namespace Nfc
{

NfcDevice::NfcDevice(nfc_device* devId)
  : DevId(devId)
{
  // TODO Auto-generated constructor stub
}

bool NfcDevice::Init()
{
  if (nfc_initiator_init(DevId) < 0)
    throw Nfc::Error(__FILE__, __LINE__);
/*
  // Drop the field for a while
  if (!nfc_configure(pnd, NDO_ACTIVATE_FIELD, false))
  {
    throw Nfc::Error(__FILE__, __LINE__);
  }
  // Let the device only try once to find a tag
  if (!nfc_configure(pnd, NDO_INFINITE_SELECT, false))
  {
    throw Nfc::Error(__FILE__, __LINE__);
  }
  if (!nfc_configure(pnd, NDO_HANDLE_CRC, true))
  {
    throw Nfc::Error(__FILE__, __LINE__);
  }
  if (!nfc_configure(pnd, NDO_HANDLE_PARITY, true))
  {
    throw Nfc::Error(__FILE__, __LINE__);
  }
  // Enable field so more power consuming cards can power themselves up
  if (!nfc_configure(pnd, NDO_ACTIVATE_FIELD, true))
  {
    throw Nfc::Error(__FILE__, __LINE__);
  }
  */
  return true;
}

std::string NfcDevice::GetName()
{
  return nfc_device_get_name(DevId);
}

std::vector<char> NfcDevice::ReadMifareUltralight()
{
  std::vector<char> ret;

  nfc_modulation nmMifare;
  nmMifare.nmt = NMT_ISO14443A;
  nmMifare.nbr = NBR_106;

  nfc_target nt;
  if (!nfc_initiator_select_passive_target(DevId, nmMifare, NULL, 0, &nt))
  {
    throw Nfc::Error(__FILE__, __LINE__);
  }

  if (nt.nti.nai.abtAtqa[1] != 0x44)
  {
    throw Nfc::Error(__FILE__, __LINE__);
  }

  return ReadMifareUltralightInternal();
}

NfcDevice::~NfcDevice()
{
  nfc_close(DevId);
}

std::vector<char> NfcDevice::ReadMifareUltralightInternal()
{
  mifareul_tag tag;
  memset(&tag, 0x00, sizeof(tag));

  uint32_t uiBlocks = 0xF;
  for (uint32_t page = 0; page <= uiBlocks; page += 4)
  {
    // Try to read out the data block
    mifare_param mp;
    if (nfc_initiator_mifare_cmd(DevId, MC_READ, page, &mp))
    {
      memcpy(tag.amb[page/4].mbd.abtData, mp.mpd.abtData, 16);
    }
    else
      throw Nfc::Error(__FILE__, __LINE__);
  }

  std::vector<char> ret;
  ret.resize(sizeof(mifareul_tag));

  memcpy(&ret[0], &tag, sizeof(mifareul_tag));
  return ret;
}

}

