/*
 * NfcDevice.h
 *
 *  Created on: Jan 20, 2013
 *      Author: gburanov
 */

#ifndef NFCDEVICE_H_
#define NFCDEVICE_H_

#include <nfc/nfc.h>

#include <string>
#include <vector>

namespace Nfc
{

class NfcDevice
{
public:
  NfcDevice(nfc_device* devId);
  virtual ~NfcDevice();

  bool Init();
  std::string GetName();

  std::vector<char> ReadMifareUltralight();

private:
  std::vector<char> ReadMifareUltralightInternal();

  nfc_device* DevId;
};

}

#endif /* NFCDEVICE_H_ */
