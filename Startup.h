/*
 * Startup.h
 *
 *  Created on: Feb 3, 2013
 *      Author: gburanov
 */

#ifndef STARTUP_H_
#define STARTUP_H_

class Startup
{
public:
  Startup(int argc, char** argv);
  virtual ~Startup();

  void Execute();
private:
  void Init(int argc, char** argv);

  bool NeedToDump;
  bool RunTests;
};

#endif /* STARTUP_H_ */
