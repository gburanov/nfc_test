/*
 * MetroParser.h
 *
 *  Created on: Jan 16, 2013
 *      Author: gburanov
 */

#ifndef METROPARSER_H_
#define METROPARSER_H_

#include <ctime>
#include <stdint.h>

#include <vector>

namespace Nfc
{
namespace Metro
{

enum TicketType
{
  UNKNOWN_TICKET,
  MOSMETRO,
  MOSGROUND,
  MOSUNIVERSAL,
  MOSMETROLIGHT,
  MOSUNIVERSALTICKET,
};

enum CardType
{
  UNKNOWN_TYPE,
  ONE_TRIP_RIDE,
  TWO_TRIP_RIDE,
  THREE_TRIP_RIDE,
  FOUR_TRIP_RIDE,
  FIVE_TRIP_RIDE,
  TEN_TRIP_RIDE,
  TWENTY_TRIP_RIDE,
  SIXTY_TRIP_RIDE,

  BAGGAGE_AND_PASS,
  BAGGAGE_ONLY,
  UNIVERSAL_ULTRALIGHT_70,
  VESB,
};

class MetroParser {
public:
	MetroParser();
	void DecodeUltralight(std::vector<char>& bytes);

	void ToString();
	void MakeDump();

	virtual ~MetroParser();

private:
	TicketType DecodeTicketType();
	CardType DecodeCardType();
	long DecodeCardNumber();
	uint8_t DecodeCardLayout();
	uint16_t DecodeRidesLeft();
	time_t DecodeIssuedAt();
	uint8_t DecodeBestInDays();
	uint16_t DecodeStationLastEnter();
	uint32_t DecodeHash();
	time_t DecodeBlankBestbefore();

	time_t GetBecomeInvalidTime();

	time_t GetTime(int daysFrom1992);

	std::vector<char> Dump;
	TicketType TType;
	CardType CType;
	long CardNumber;
	uint8_t CardLayout;
	uint16_t RidesLeft;
	time_t IssuedAt;
	time_t BecomeInvalidTime;
	uint16_t LastStation;
	uint32_t Hash;
};

}
}

#endif /* METROPARSER_H_ */
