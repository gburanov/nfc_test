/*
 * tests.cpp
 *
 *  Created on: Jan 19, 2013
 *      Author: gburanov
 */

#include "MetroParserTests.h"

namespace Nfc
{
namespace Tests
{

void StartTests()
{
  MetroParserTests tests;
  tests.Run();
}

}
}
