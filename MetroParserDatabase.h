/*
 * MetroParserDatabase.h
 *
 *  Created on: Feb 3, 2013
 *      Author: gburanov
 */

#ifndef METROPARSERDATABASE_H_
#define METROPARSERDATABASE_H_

#include <string>
#include <map>

namespace Nfc
{

namespace Metro
{

  class MetroParserDatabase
  {
    public:
      MetroParserDatabase();
      virtual ~MetroParserDatabase();

      std::string GetStationName(int station);
    private:
      void InitStations(std::map<int, std::string>& stations);
  };

}
}
#endif /* METROPARSERDATABASE_H_ */
